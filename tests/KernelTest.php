<?php

namespace App\Tests;

use App\Config;
use App\Container\GenericContainer;
use App\Kernel;
use PHPUnit\Framework\TestCase;

class KernelTest extends TestCase
{
    public function test_kernel()
    {
        $container = new GenericContainer();
        $kernel = new Kernel($container);

        $config = $kernel->container->get(Config::class);
        $this->assertEquals(3306, $config->databasePort);
    }
}

<?php

namespace App\Tests\Container;

use App\Container\GenericContainer;
use App\Container\UnknownParameterException;
use PHPUnit\Framework\TestCase;

class GenericContainerTest extends TestCase
{
    public function test_container()
    {
        $container = new GenericContainer();

        $d = $container->get(DependencyA::class);
        $this->assertInstanceOf(DependencyA::class, $d);
    }

    public function test_container_parameters()
    {
        $container = new GenericContainer();
        $container->registerParameter('some_string', 'some string');

        $this->assertEquals('some string', $container->getParameter('some_string'));
    }

    public function test_container_unknown_parameter()
    {
        $container = new GenericContainer();
        $this->ExpectException(UnknownParameterException::class);
        $container->getParameter('some_string');
    }

    public function test_container_autowire()
    {
        $container = new GenericContainer();
        $container->registerParameter('some_string', 'some string');
        $container->registerParameter('hans', [123]);

        $d = $container->get(DependencyB::class);
        $this->assertInstanceOf(DependencyB::class, $d);
        $this->assertInstanceOf(DependencyA::class, $d->dependencyA);
    }
}

class DependencyA {}
class DependencyB {
    public function __construct(
        public array $hans,
        public DependencyA $dependencyA,
        public string $some_string,
        protected int $some_int = 123,
    ) {}
}

<?php

namespace App\Interfaces;

interface ContainerInterface
{
    public function register(string $className, callable $definition): self;
    public function registerParameter(string $parameterName, mixed $value): self;


    /**
     * @template TClassName
     * @param class-string<TClassName> $className
     * @return TClassName
     */
    public function get(string $className): object;

    /**
     * @param string $parameterName
     * @return object|array|string|int|bool
     */
    public function getParameter(string $parameterName): object|array|string|int|bool;
}

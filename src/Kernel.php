<?php

namespace App;

use App\Interfaces\ContainerInterface;

class Kernel
{
    public function __construct(
        public readonly ContainerInterface $container,
    ) {}
}

<?php

namespace App;
class Config extends InstallationConfig
{
    public int $databasePort = 3306;
    public string $databaseCharset = 'utf8mb4';
    public string $databaseCollation = 'utf8mb4_unicode_ci';
}

<?php

namespace App\Container;

use App\Interfaces\ContainerInterface;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;
use function PHPUnit\Framework\matches;

final class GenericContainer implements ContainerInterface
{
    private array $definitions = [];
    private array $parameters = [];

    public function register(string $className, callable $definition): self
    {
        $this->definitions[$className] = $definition;

        return $this;
    }

    public function registerParameter(string $parameterName, mixed $value): self
    {
        $this->parameters[$parameterName] = $value;

        return $this;
    }

    public function get(string $className): object
    {
        // https://stitcher.io/blog/new-in-php-81#first-class-callable-syntax-rfc
        $definition = $this->definitions[$className] ?? $this->autowire(...);

        return $definition($className);
    }

    /**
     * @throws UnknownParameterException
     */
    public function getParameter(string $parameterName): string|int|array|bool
    {
        if (!array_key_exists($parameterName, $this->parameters)) {
            throw new UnknownParameterException("Parameter '{$parameterName}' is not registered");
        }

        return $this->parameters[$parameterName];
    }

    /**
     * @throws ReflectionException
     * @throws UnknownParameterException
     */
    private function autowire(string $className): object
    {
        $reflection = new ReflectionClass($className);

        $parameters = array_map(
            function(ReflectionParameter $parameter) {
                return match($parameter->getType()->getName()) {
                    'string', 'array', 'int' => $this->getParameterValue($parameter),
                    default => $this->get($parameter->getType()->getName())
                };
            },
            $reflection->getConstructor()?->getParameters() ?? []
        );

        return new $className(...$parameters);
    }

    /**
     * @throws UnknownParameterException
     */
    private function getParameterValue(ReflectionParameter $parameter): object|string|int|array|bool
    {
        try {
            return $this->getParameter($parameter->getName());
        } catch (UnknownParameterException) {
            if ($parameter->isDefaultValueAvailable()) {
                return $parameter->getDefaultValue();
            }
        }

        throw new UnknownParameterException("Parameter '{$parameter->getName()}' is not registered");
    }
}
